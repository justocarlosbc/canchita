CREATE TABLE PERSONA (
  id_persona INTEGER   NOT NULL ,
  tip_doc INTEGER    ,
  num_doc VARCHAR(20)    ,
  nombre VARCHAR(50)    ,
  apellido VARCHAR(50)    ,
  tip_persona INTEGER    ,
  fecha_nacimiento DATE    ,
  felefono1 VARCHAR(20)    ,
  felefono2 VARCHAR(20)    ,
  correo VARCHAR(100)    ,
  dirreccion VARCHAR(150)      ,
PRIMARY KEY(id_persona));

COMMENT ON COLUMN PERSONA.tip_doc IS 'dni,ruc,carnet de extrageria,etc';
COMMENT ON COLUMN PERSONA.tip_persona IS 'Natural, juridica';



CREATE TABLE PRODUCTO (
  id_producto INTEGER   NOT NULL ,
  nombre VARCHAR(100)    ,
  descripcion VARCHAR(200)    ,
  estado INTEGER    ,
  fecha_creacion DATE    ,
  fecha_modificacion DATE      ,
PRIMARY KEY(id_producto));




CREATE TABLE DESCUENTOS (
  id_descuentos INTEGER   NOT NULL ,
  codigo_desc VARCHAR(100)   NOT NULL ,
  fecha_ini_vigen DATE   NOT NULL ,
  fecha_fin_vigen DATE    ,
  estado INTEGER    ,
  tipo_desc INTEGER    ,
  unidad_desc INTEGER      ,
PRIMARY KEY(id_descuentos));

COMMENT ON COLUMN DESCUENTOS.tipo_desc IS 'determina si es monto o porcentaje';



CREATE TABLE SUCURSALES (
  id_sucursal INTEGER   NOT NULL ,
  PERSONA_id_empresa INTEGER   NOT NULL ,
  serie_sucursal VARCHAR(3)   NOT NULL ,
  direccion INTEGER    ,
  telefono VARCHAR(20)    ,
  tipo_sucursal INTEGER    ,
  fecha_creacion DATE      ,
PRIMARY KEY(id_sucursal)  ,
  FOREIGN KEY(PERSONA_id_empresa)
    REFERENCES PERSONA(id_persona));


CREATE INDEX SUCURSALES_FKIndex1 ON SUCURSALES (PERSONA_id_empresa);


CREATE INDEX IFK_Rel_03 ON SUCURSALES (PERSONA_id_empresa);


CREATE TABLE CANCHAS (
  id_cancha INTEGER   NOT NULL ,
  SUCURSALES_id_sucursal INTEGER   NOT NULL ,
  nombre VARCHAR(50)    ,
  capacidad INTEGER    ,
  dimen_largo DOUBLE    ,
  dimen_ancho DOUBLE    ,
  tipo_cancha INTEGER    ,
  ubicacion VARCHAR(50)      ,
PRIMARY KEY(id_cancha)  ,
  FOREIGN KEY(SUCURSALES_id_sucursal)
    REFERENCES SUCURSALES(id_sucursal));


CREATE INDEX CANCHAS_FKIndex1 ON CANCHAS (SUCURSALES_id_sucursal);

COMMENT ON COLUMN CANCHAS.capacidad IS 'jugadores';

CREATE INDEX IFK_Rel_04 ON CANCHAS (SUCURSALES_id_sucursal);


CREATE TABLE USUARIO (
  id_usuario INTEGER   NOT NULL ,
  PERSONA_id_empresa INTEGER   NOT NULL ,
  PERSONA_id_persona INTEGER   NOT NULL ,
  usuario VARCHAR(20)    ,
  password_2 VARCHAR(12)    ,
  tipo_usuario INTEGER      ,
PRIMARY KEY(id_usuario)    ,
  FOREIGN KEY(PERSONA_id_persona)
    REFERENCES PERSONA(id_persona),
  FOREIGN KEY(PERSONA_id_empresa)
    REFERENCES PERSONA(id_persona));


CREATE INDEX USUARIO_FKIndex1 ON USUARIO (PERSONA_id_persona);
CREATE INDEX USUARIO_FKIndex2 ON USUARIO (PERSONA_id_empresa);


CREATE INDEX IFK_Rel_10 ON USUARIO (PERSONA_id_persona);
CREATE INDEX IFK_Rel_11 ON USUARIO (PERSONA_id_empresa);


CREATE TABLE HORARIO_CANCHA (
  id_hora_cancha INTEGER   NOT NULL ,
  PRODUCTO_id_producto INTEGER   NOT NULL ,
  CANCHAS_id_cancha INTEGER   NOT NULL ,
  hora_inicio TIME    ,
  hora_fin TIME    ,
  dia_atencion INTEGER    ,
  precio DOUBLE    ,
  min_reserva DOUBLE    ,
  estado INTEGER    ,
  fecha_creacion DATE    ,
  fecha_actualizacion DATE      ,
PRIMARY KEY(id_hora_cancha)    ,
  FOREIGN KEY(CANCHAS_id_cancha)
    REFERENCES CANCHAS(id_cancha),
  FOREIGN KEY(PRODUCTO_id_producto)
    REFERENCES PRODUCTO(id_producto));


CREATE INDEX HORARIO_CANCHA_FKIndex1 ON HORARIO_CANCHA (CANCHAS_id_cancha);
CREATE INDEX HORARIO_CANCHA_FKIndex2 ON HORARIO_CANCHA (PRODUCTO_id_producto);


CREATE INDEX IFK_Rel_05 ON HORARIO_CANCHA (CANCHAS_id_cancha);
CREATE INDEX IFK_Rel_12 ON HORARIO_CANCHA (PRODUCTO_id_producto);


CREATE TABLE RESERVA_CANCHA (
  id_reserva_cancha INTEGER   NOT NULL ,
  DESCUENTOS_id_descuentos INTEGER   NOT NULL ,
  HORARIO_CANCHA_id_hora_cancha INTEGER   NOT NULL ,
  CANCHAS_id_cancha INTEGER   NOT NULL ,
  USUARIO_id_usuario INTEGER   NOT NULL ,
  hora_inicio TIME    ,
  hora_fin TIME    ,
  monto_total_facturado DOUBLE    ,
  monto_total_real DOUBLE    ,
  monto_reserva DOUBLE    ,
  monto_pendiente DOUBLE    ,
  estado INTEGER      ,
PRIMARY KEY(id_reserva_cancha)        ,
  FOREIGN KEY(USUARIO_id_usuario)
    REFERENCES USUARIO(id_usuario),
  FOREIGN KEY(CANCHAS_id_cancha)
    REFERENCES CANCHAS(id_cancha),
  FOREIGN KEY(HORARIO_CANCHA_id_hora_cancha)
    REFERENCES HORARIO_CANCHA(id_hora_cancha),
  FOREIGN KEY(DESCUENTOS_id_descuentos)
    REFERENCES DESCUENTOS(id_descuentos));


CREATE INDEX RESERVA_CANCHA_FKIndex1 ON RESERVA_CANCHA (USUARIO_id_usuario);
CREATE INDEX RESERVA_CANCHA_FKIndex2 ON RESERVA_CANCHA (CANCHAS_id_cancha);
CREATE INDEX RESERVA_CANCHA_FKIndex3 ON RESERVA_CANCHA (HORARIO_CANCHA_id_hora_cancha);
CREATE INDEX RESERVA_CANCHA_FKIndex4 ON RESERVA_CANCHA (DESCUENTOS_id_descuentos);

COMMENT ON COLUMN RESERVA_CANCHA.monto_total_facturado IS 'con descuento';
COMMENT ON COLUMN RESERVA_CANCHA.monto_total_real IS 'sin descuento';
COMMENT ON COLUMN RESERVA_CANCHA.estado IS 'reservado,cancelado,pagado';

CREATE INDEX IFK_Rel_06 ON RESERVA_CANCHA (USUARIO_id_usuario);
CREATE INDEX IFK_Rel_07 ON RESERVA_CANCHA (CANCHAS_id_cancha);
CREATE INDEX IFK_Rel_08 ON RESERVA_CANCHA (HORARIO_CANCHA_id_hora_cancha);
CREATE INDEX IFK_Rel_13 ON RESERVA_CANCHA (DESCUENTOS_id_descuentos);



